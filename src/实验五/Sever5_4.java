package 实验五;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
public class Sever5_4 {
    public static void main(String[] args) {
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try{
            serverForClient = new ServerSocket(2010);
        }catch (IOException e){
            System.out.println("双方已断开连接"+e);
        }
        try{
            System.out.println("准备接受对方传来的问题");
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            Key_DH.DH("Wpub.dat","Wpri.dat");
            int length = Integer.parseInt(in.readUTF());
            byte cpub[] = new byte[length];
            for(int i=0;i<length;i++) {
                String temp = in.readUTF();
                cpub[i] = Byte.parseByte(temp);
            }
            ByteArrayInputStream ckey1 = new ByteArrayInputStream(cpub);
            ObjectInputStream ckey = new ObjectInputStream(ckey1);
            Key k1 = (Key) ckey.readObject();
            FileOutputStream f2 = new FileOutputStream("Lpub.dat");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k1);
            FileInputStream my = new FileInputStream("Wpub.dat");
            ObjectInputStream mypub = new ObjectInputStream(my);
            Key kp = (Key) mypub.readObject();
            ByteArrayOutputStream DH = new ByteArrayOutputStream();
            ObjectOutputStream myDH = new ObjectOutputStream(DH);
            myDH.writeObject(kp);
            byte []pub = DH.toByteArray();
            out.writeUTF(pub.length+"");
            for(int i=0;i<pub.length;i++) {
                out.writeUTF(pub[i]+ "");
            }
            KeyAgree.Agree("Lpub.dat","Wpri.dat");
            FileInputStream f = new FileInputStream("sb.dat");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.println("公共密钥为：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+" ");
            }
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            String clength = in.readUTF();
            byte ctext[] = new byte[Integer.parseInt(clength)];
            for (int i = 0; i < Integer.parseInt(clength); i++) {
                String temp = in.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            byte[] ptext = cp.doFinal(ctext);
            String suffix = new String(ptext, "UTF8");
            String [] str = suffix.split(" ");
            System.out.println("\n收到问题，正在解密后缀表达式:"+suffix);
            System.out.println("已传输得出结果：");
            MyDC myDC = new MyDC(str);
            String answer =  String.valueOf(myDC.answer);
            out.writeUTF(answer+"");
            Thread.sleep(500);
        }catch (Exception e){
            System.out.println("双方已断开连接");
        }
    }
}