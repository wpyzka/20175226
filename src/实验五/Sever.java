package 实验五;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import java.io.*;
import java.net.*;
public class Sever {
    public static void main(String[] args) {
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;

        try{
            serverForClient = new ServerSocket(2010);
        }catch (IOException e){
            System.out.println("双方已断开连接"+e);
        }
        try{
            System.out.println("准备接受对方传来的问题");
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            String suffix = in.readUTF();
            String [] str = suffix.split(" ");
            System.out.println("收到问题，正在解决:"+suffix);
            System.out.println("已传输得出结果：");
            MyDC myDC = new MyDC(str);
            String answer =  String.valueOf(myDC.answer);
            out.writeUTF(answer+"");
            Thread.sleep(500);
        }catch (Exception e){
            System.out.println("双方已断开连接");
        }
    }
}