package 实验五;

import java.util.Stack;
public class MyDC{
    public int answer;
    public MyDC (String[] args) {
        Stack<String> z = new Stack<String>();
        int num1,num2,d;
        for(int i=0;i<args.length;i++) {
            switch (args[i]){
                case"+":
                    num2 = Integer.valueOf(z.pop());
                    num1 = Integer.valueOf(z.pop());
                    d = num1+num2;
                    z.push(String.valueOf(d));
                    break;
                case"-":
                    num2 = Integer.valueOf(z.pop());
                    num1 = Integer.valueOf(z.pop());
                    d = num1-num2;
                    z.push(String.valueOf(d));
                    break;
                case"*":
                    num2 = Integer.valueOf(z.pop());
                    num1 = Integer.valueOf(z.pop());
                    d = num1*num2;
                    z.push(String.valueOf(d));
                    break;
                case"/":
                    num2 = Integer.valueOf(z.pop());
                    num1 = Integer.valueOf(z.pop());
                    d = num1/num2;
                    z.push(String.valueOf(d));
                    break;
                case"":
                case" ":
                    break;

                default:
                    z.push(args[i]);
                    break;
            }
        }
        while (z.empty() == false) {
            answer =  Integer.valueOf(z.pop());
        }
        System.out.println(answer);
    }
}