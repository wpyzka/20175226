package 实验五;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import java.io.*;
import java.net.*;
import javax.crypto.*;
import javax.crypto.spec.*;
public class Sever5_3 {
    public static void main(String[] args) {
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try{
            serverForClient = new ServerSocket(2010);
        }catch (IOException e){
            System.out.println("双方已断开连接"+e);
        }
        try{
            System.out.println("准备接受对方传来的问题");
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            String keylength = in.readUTF();
            byte []kb = new byte[Integer.parseInt(keylength)];
            System.out.println("收到的密钥为：");
            for(int i = 0;i<Integer.parseInt(keylength);i++) {
                String str = in.readUTF();
                kb[i] = Byte.parseByte(str);
                System.out.print(kb[i] + " ");
            }
            SecretKeySpec k = new SecretKeySpec(kb, "DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            String clength = in.readUTF();
            byte ctext[] = new byte[Integer.parseInt(clength)];
            for (int i = 0;i<Integer.parseInt(clength);i++) {
                String temp = in.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            byte[] ptext = cp.doFinal(ctext);
            String suffix = new String(ptext,"UTF8");
            String [] str = suffix.split(" ");
            System.out.println("\n收到问题，解密后的后缀表达式为:"+suffix);
            System.out.println("已传输得出结果：");
            MyDC myDC = new MyDC(str);
            String answer =  String.valueOf(myDC.answer);
            out.writeUTF(answer+"");
            Thread.sleep(500);
        }catch (Exception e){
            System.out.println("双方已断开连接");
        }
    }
}