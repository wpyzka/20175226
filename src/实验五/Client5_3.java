package 实验五;

import java.io.*;
import java.net.*;
import javax.crypto.*;
import java.util.Scanner;

public class Client5_3 {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        try{
            mysocket = new Socket("169.254.92.198",2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            KeyGenerator kg = KeyGenerator.getInstance("DESede");
            kg.init(168);
            SecretKey k = kg.generateKey();
            byte []kb = k.getEncoded();
            out.writeUTF(kb.length+ "");
            System.out.println("产生的密钥为");
            for(int i=0;i<kb.length;i++) {
                System.out.print(kb[i]+ " ");
                out.writeUTF(kb[i] +"");
            }
            System.out.println("\n请输入中缀表达式：");
            String infix = inn.nextLine();
            String [] str = infix.split(" ");
            System.out.println("后缀表达式为");
            MyBC suffix =new MyBC(str);
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE,k);
            byte ptext[] = suffix.jieguo.getBytes("UTF8");
            byte ctext[] = cp.doFinal(ptext);
            out.writeUTF(ctext.length + "");
            for(int i=0;i<ctext.length;i++) {
                out.writeUTF(ctext[i] +"");
            }
            String result = in.readUTF();
            System.out.println("已收到计算结果"+result);
            Thread.sleep(500);
        }catch (Exception e){
            System.out.println("双方已断开连接"+e);
        }
    }
}