package 实验五;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Scanner inn = new Scanner(System.in);
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        try{
            mysocket = new Socket("169.254.92.198",2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("请输入中缀表达式：");
            String infix = inn.nextLine();
            String [] str = infix.split(" ");
            System.out.println("已传递后缀表达式");
            MyBC suffix =new MyBC(str);
            out.writeUTF(suffix.jieguo);
            String result = in.readUTF();
            System.out.println("已收到计算结果"+result);
            Thread.sleep(500);
        }catch (Exception e){
            System.out.println("双方已断开连接"+e);
        }
    }
}