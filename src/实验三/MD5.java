package 实验三;
import java.io.UnsupportedEncodingException;
import java.security.*;

public class MD5{
    public static void main(String args[]) throws Exception {
        String originalStr = args[0];
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            m.update(originalStr.getBytes("UTF8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte byteStr[] = m.digest();
        GetResult getResult = new GetResult();
        for (int i = 0; i < byteStr.length; i++) {
            getResult.result += Integer.toHexString((0x000000ff & byteStr[i]) |
                    0xffffff00).substring(6);
        }
        System.out.println(getResult.result);
    }

    public static class GetResult {
        String result = "";
    }
}