interface Advertisement {
    public void showAdvertisement();
    public String getCorpName();
}

class AdvertisementBoard {
    public void show(Advertisement adver) {
        System.out.println(adver.getCorpName()+"的广告词如下:");
        adver.showAdvertisement();
    }
}

class BlackLandCorp implements Advertisement {
    public void showAdvertisement(){
        System.out.println("**************");
        System.out.printf("劳动是爹\n土地是妈\n");
        System.out.println("**************");
    }
    public String getCorpName() {
        return "黑土集团" ;
    }
}

class WhiteCloudCorp implements Advertisement { //PhilipsCorpʵ��Avertisement�ӿ�
    public void showAdvertisement(){
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
        System.out.printf("飞机中的战斗机，哎yes!\n");
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
    }
    public String getCorpName() {
        return "白云有限公司" ;
    }
}

class WPYCorp implements Advertisement {
    public void showAdvertisement(){
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
        System.out.printf("鹏之背若泰山，翼若垂天之雲\n");
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@");
    }
    public String getCorpName() {
        return "鹏雲有限公司" ;
    }
}

public class Example6_6 {
    public static void main(String args[]) {
        AdvertisementBoard board = new AdvertisementBoard();
        board.show(new BlackLandCorp());
        board.show(new WhiteCloudCorp());
        board.show(new WPYCorp());
    }
}
