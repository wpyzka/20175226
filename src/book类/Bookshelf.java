package book类;
import java.util.*;
class Book {
    String name;
    String author;
    String push;
    String date;
    String book;
    Book() {}
    void Book(String name) {}
    public void setterBook(String name,String author,String push,String date) {
        this.name = name;
        this.author = author;
        this.push = push;
        this.date = date;
    }
    public String getterBook() {
        return book = this.name + "、" + this.author + "、" + this.push + "、" + this.date + '、';
    }
    public String toString() {
        return "此书信息包括："+book ;
    }
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Book)) {
            return false;
        }
        Book te = (Book) obj;
        if (te.book.equals(book)) {
            return true;
        }
        return false;
    }
}

public class Bookshelf {
    public static void main(String[] args) {
        Book b1 = new Book();
        Book b2 = new Book();
        Book b3 = new Book();
        b1.setterBook("Java2实用教程（第五版）","耿祥义","清华大学出版社","2017年1月");
        b2.setterBook("密码学","郑秀林","金城出版社","2016年8月");
        b3.setterBook("计算机网络","谢希仁","电子工业出版社","2017年1月");
        System.out.println(b1.getterBook());
        System.out.println(b2.getterBook());
        System.out.println(b3.getterBook());
        System.out.println(b2.equals(b1));
        System.out.println(b2.equals(b2));
        System.out.println(b2.equals(b3));
    }
}
