package shiyan2;

// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=20175226%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Byte extends  Data {
   byte value;
   Byte() {
        value=20175226%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Short extends  Data {
    short value;
    Short() {
        value=20175226%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Boolean extends  Data {
    boolean value;
    Boolean() {
        value=20175226%6==4;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Long extends  Data {
    long value;
    Long() {
        value=20175226;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Double extends  Data {
    double value;
    Double() {
        value=20175226%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Float extends  Data {
    float value;
    Float() {
        value=20175226%6;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
class ShortFactory extends Factory {
    public Data CreateDataObject(){
        return new Short();
    }
}
class BoolenFactory extends Factory {
    public Data CreateDataObject(){
        return new Boolean();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
class DoubletFactory extends Factory {
    public Data CreateDataObject(){
        return new Double();
    }
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Float();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        d = new Document(new ByteFactory());
        d.DisplayData();
        d = new Document(new ShortFactory());
        d.DisplayData();
        d = new Document(new BoolenFactory());
        d.DisplayData();
        d = new Document(new LongFactory());
        d.DisplayData();
        d = new Document(new DoubletFactory());
        d.DisplayData();
        d = new Document(new FloatFactory());
        d.DisplayData();
    }
}