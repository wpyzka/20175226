interface SpeakHello2 {
    void speak();
}
class  HelloMachine {
    public void turnOn(SpeakHello2 hello) {
        hello.speak();
    }
}
public class Example7_3 {
    public static void main(String args[]) {
        HelloMachine machine = new HelloMachine();
        machine.turnOn( new SpeakHello2(){
                            public void speak() {
                                System.out.println("hello,you are welcome!");
                            }
                        }
        );
        machine.turnOn( new SpeakHello2(){
                            public void speak() {
                                System.out.println("你好，欢迎光临!");
                            }
                        }
        );
    }
}

